﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

using BulletProof.Models;
using BulletProof.DAL;
using System.Data.SqlClient;
using Microsoft.AspNet.Identity.EntityFramework;

using System.Net.Mail;

namespace BulletProof.Controllers
{
    public class ManageController : Controller
    {
        private string pathToLocalData = Path.GetTempPath() + @"\BulletProof\";
        private const string connectionString = @"Data Source=.\SQLExpress;Integrated Security=true;AttachDbFilename=/httpdocs/App_Data/aspnet-BulletProof-20140416025851.mdf;User Instance=true;";

        public ActionResult Index()
        {
            string username = User.Identity.GetUserName();
            string userID = User.Identity.GetUserId();
            
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser user = userManager.FindByNameAsync(User.Identity.Name).Result;

            if (user != null)
            {
                ViewData["name"] = user.UserName;
                ViewData["emailAddress"] = user.EmailAddress;
                ViewData["uploadedFiles"] = this.getUploadedFiles(user, userID, user.EmailAddress);
                ViewData["role"] = user.Role;
                ViewData["loggedIn"] = true;
            }
            else
            {
                ViewData["loggedIn"] = false;
            }
            this.uploadFiles(user, userID);
            return View();
        }

        public FileStreamResult Download(string filepath, string filename)
        {
            String[] filenameSplit = filename.Split('.');
            String[] filepathSplit = filepath.Split('\\');
            String userGUID = filepathSplit[filepathSplit.Length - 2];
            String fileExtension = filenameSplit[1];

            String saveAsFileName = filenameSplit[0];

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser user = userManager.FindByNameAsync(User.Identity.Name).Result;

            if (user.Role != UserRoles.STUDENT)
            {
                saveAsFileName += "_" + userGUID;
            }

            return File(new FileStream(filepath, FileMode.Open), "application/" + fileExtension, saveAsFileName);
        }

        public String[] parseFileDirectory(String fileDirectory)
        {
            String[] parsedFileDirectory = new String[2] ;
            parsedFileDirectory[1] = fileDirectory;
            string[] parts = fileDirectory.Split('\\');
            int partsCount = parts.Length;
            parsedFileDirectory[0] = parts[partsCount - 1];

            return parsedFileDirectory;
        }

        public List<String[]> getUploadedFiles(ApplicationUser user, string userID, string studentEmailAddress)
        {
            List<String[]> uploadedFiles = new List<String[]>();
            String[] fileDirectories;

            if (user.Role == UserRoles.STUDENT)
            {
                if (Directory.Exists(pathToLocalData + userID + @"\"))
                {
                    fileDirectories = Directory.GetFiles(pathToLocalData + userID + @"\");
                }
                else
                {
                    Directory.CreateDirectory(pathToLocalData + userID + @"\");
                    fileDirectories = null;
                }
            }
            else
            {
                fileDirectories = Directory.GetFiles(pathToLocalData, "*", SearchOption.AllDirectories );
                
                // TODO :: [pls send email]
            }

            foreach (String fileDirectory in fileDirectories)
            {
                uploadedFiles.Add(this.parseFileDirectory(fileDirectory));
            }
            
            return uploadedFiles;
        }
        

        public void uploadFiles(ApplicationUser user, string userID)
        {
            foreach (string upload in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[upload];

                if (hpf.ContentLength != 0)
                {
                    if (user.Role == UserRoles.STUDENT)
                    {
                        string savedFileName = pathToLocalData + userID + @"\";
                        Directory.CreateDirectory(savedFileName);
                        savedFileName += hpf.FileName;
                        hpf.SaveAs(savedFileName);


                        DateTime currentDateTime = DateTime.Now;

                        using (var connection = new SqlConnection(connectionString))
                        {
                            var query = "INSERT INTO Files (Name, Path, StudentEmail, DateAdded) VALUES (@Filename, @Filepath, @StudentEmail, @DateAdded)";
                            var cmd = new SqlCommand(query, connection);

                            cmd.Parameters.AddWithValue("@Filename", hpf.FileName);
                            cmd.Parameters.AddWithValue("@Filepath", savedFileName);
                            cmd.Parameters.AddWithValue("@StudentEmail", user.EmailAddress);
                            cmd.Parameters.AddWithValue("@DateAdded", currentDateTime);
                            connection.Open();
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        String[] uploadNameSplit = hpf.FileName.Split('_');
                        String[] splitAgainSplit = uploadNameSplit[1].Split('.');

                        String fileExtension = splitAgainSplit[1];
                        String userGUID = splitAgainSplit[0];
                        String filename = uploadNameSplit[0];

                        String fullFilename = filename + "." +  fileExtension;

                        String fullpath = pathToLocalData + userGUID + @"\" + fullFilename;

                        using (var connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            var query = "UPDATE Files SET Marked=@Marked, MarkerEmail=@MarkerEmail" + " WHERE Path=@Filepath";
                            var cmd = new SqlCommand(query, connection);

                            cmd.Parameters.AddWithValue("@Marked", 1);
                            cmd.Parameters.AddWithValue("@MarkerEmail", user.EmailAddress);
                            cmd.Parameters.AddWithValue("@Filepath", fullpath);

                            cmd.ExecuteNonQuery();
                        }

                        hpf.SaveAs(fullpath);

                    }
                }

                // [TODO:: make a try/catch here to ensure stability]


            }

        }
	}
}