﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BulletProof.Startup))]
namespace BulletProof
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
