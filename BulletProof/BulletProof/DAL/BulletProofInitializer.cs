﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BulletProof.Models;

namespace BulletProof.DAL
{
    public class BulletProofInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<BulletProofContext>
    {
        protected override void Seed(BulletProofContext context)
        {
            var users = new List<ApplicationUser>
            {
                new ApplicationUser{UserName="faharn.wali@gmail.com", Role=UserRoles.STUDENT, PasswordHash=""},
                new ApplicationUser{UserName="erichislop@hotmail.com", Role=UserRoles.STUDENT},
                new ApplicationUser{UserName="ryan@outlook.com", Role=UserRoles.MARKER}
            };

            users.ForEach(u => context.Users.Add(u));
            context.SaveChanges();
        }
    }
}