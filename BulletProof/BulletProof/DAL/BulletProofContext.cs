﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

using BulletProof.Models;

namespace BulletProof.DAL
{
    public class BulletProofContext : DbContext
    {

        public BulletProofContext() : base("BulletProofContext")
        {
        }

        public DbSet<ApplicationUser>   Users { get; set; }
        public DbSet<File>              Files { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }

    }
}