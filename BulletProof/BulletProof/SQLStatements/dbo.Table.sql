﻿CREATE TABLE [dbo].[Table]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Path] VARCHAR(MAX) NOT NULL, 
    [Description] TEXT NULL, 
    [MarkerEmail] NVARCHAR(50) NULL, 
    [StudentEmail] NVARCHAR(50) NOT NULL, 
    [Marked] INT NULL, 
    [DateAdded] DATETIME NOT NULL, 
    [DateMarked] DATETIME NULL
)
