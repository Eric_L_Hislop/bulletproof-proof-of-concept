﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace BulletProof.Models
{
    public enum UserRoles
    {
        STUDENT, MARKER, ADMIN
    }

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public UserRoles? Role { get; set; }

        public string EmailAddress { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("BulletProofContext")    // [FARHAN] [IFFY]
        {
        }
    }
}