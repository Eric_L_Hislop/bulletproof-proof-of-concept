﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BulletProof.Models
{
    public class File
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }

        public string MarkerEmail { get; set; }
        public string StudentEmail { get; set; }

        public Boolean Marked { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateMarked { get; set; }
    }
}